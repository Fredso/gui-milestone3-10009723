﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

using System.Data;
using MySql.Data.Common;
using MySql.Data.Types;

namespace MySQLDemo.Classes
{
    public class MySQLCustom
    {
        //CHANGE THESE TO MATCH YOUR DB
        private static string host = "localhost";
        private static string user = "root";
        private static string pass = "SaQpLl2016";
        private static string dbname = "gui_comp6001_16b_assn3";

        //DO NOT CHANGE THESE
        private static MySqlConnection Connection(string h, string u, string p, string n)
        {
            string serverConnection = String.Format($"Server={h};Database={n};Uid={u};Pwd={p};SslMode=None;charset=utf8");

            MySqlConnection connection = new MySqlConnection(serverConnection);

            return connection;
        }

        public static MySqlConnection conn()
        {
            var a = Connection(host, user, pass, dbname);
            return a;
        }

        public class Results
        {
            public string Row { get; set; }
        }

        public static List<Results> SelectCommand(string command, MySqlConnection connection)
        {
            //Fixes Encoding - Courtsey of Stackoverflow :-)
            EncodingProvider p;
            p = CodePagesEncodingProvider.Instance;
            Encoding.RegisterProvider(p);

            List<Results> listTables = new List<Results>();

            using (connection)
            {
                connection.Open();

                MySqlCommand getCommand = connection.CreateCommand();
                getCommand.CommandText = command;
                using (MySqlDataReader reader = getCommand.ExecuteReader())
                {
                    while(reader.Read())
                    {
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            listTables.Add(new Results() { Row = $"{reader.GetValue(i)}"});
                        }
                    }
                }
            }

            return listTables;
        }

        public static List<string> ShowInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return dataList;
        }

        public static string ShowInString(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i ++)
            {
                dataList.Add($"{list[i].Row}");
            }

            return string.Join(",", dataList);
        }

        //It creates a list with Surnames, Names *****************************
        public static List<string> ShowLnameNameInList(string command)
        {
            var dataList = new List<string>();
            var list = new List<Results>(SelectCommand(command, conn()));

            for (var i = 0; i < list.Count; i=i+2)
            {
                dataList.Add($"{list[i+1].Row}, {list[i].Row}");
            }

            return dataList;
        }


        //////////////////    DB CLASS ///////////////////////////

        //CHANGE THIS!!
        //This class is specific to your DB
        public class AddPerson
        {
            public string fname { get; set; }
            public string lname { get; set; }
            public DateTime dob { get; set; }
            public int id { get; set; }
            public int stNumber { get; set; }
            public string stName { get; set; }
            public string postCode { get; set; }
            public string city { get; set; }
            public string phone1 { get; set; }
            public string phone2 { get; set; }
            public string email { get; set; }
            public AddPerson(string _fname, string _lname, DateTime _dob, int _stNumber, string _stName, string _postCode, string _city, string _phone1, string _phone2, string _email)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                stNumber = _stNumber;
                stName = _stName;
                postCode = _postCode;
                city = _city;
                phone1 = _phone1;
                phone2 = _phone2;
                email = _email;
            }

            public AddPerson(int _id)
            {
                id = _id;
            }

            public AddPerson(string _fname, string _lname, DateTime _dob, int _id, int _stNumber, string _stName, string _postCode, string _city, string _phone1, string _phone2, string _email)
            {
                fname = _fname;
                lname = _lname;
                dob = _dob;
                id = _id;
                stNumber = _stNumber;
                stName = _stName;
                postCode = _postCode;
                city = _city;
                phone1 = _phone1;
                phone2 = _phone2;
                email = _email;
            }
        }

        ////////////////////  INSERT STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void AddData(string fname, string lname, string dob, int stNumber, string stName, string postCode, string city, string phone1, string phone2, string email)
        {
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date, stNumber, stName, postCode, city, phone1, phone2, email);

            var con = conn();
            con.Open();

            MySqlCommand insertCommand = con.CreateCommand();

            insertCommand.CommandText = "INSERT INTO tbl_people(FNAME, LNAME, DOB, Str_NUMBER, Str_NAME, POSTCODE, CITY, PHONE1, PHONE2, EMAIL) VALUES(@fname, @lname, @dob, @stNumber, @stName, @postCode, @city, @phone1, @phone2, @email)";
            insertCommand.Parameters.AddWithValue("@fname", customdb.fname);
            insertCommand.Parameters.AddWithValue("@lname", customdb.lname);
            insertCommand.Parameters.AddWithValue("@dob", customdb.dob);
            insertCommand.Parameters.AddWithValue("@stNumber", customdb.stNumber);
            insertCommand.Parameters.AddWithValue("@stName", customdb.stName);
            insertCommand.Parameters.AddWithValue("@postCode", customdb.postCode);
            insertCommand.Parameters.AddWithValue("@city", customdb.city);
            insertCommand.Parameters.AddWithValue("@phone1", customdb.phone1);
            insertCommand.Parameters.AddWithValue("@phone2", customdb.phone2);
            insertCommand.Parameters.AddWithValue("@email", customdb.email);
            insertCommand.ExecuteNonQuery();

            con.Clone();
        }



        ////////////////////  UPDATE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void UpdateData(string fname, string lname, string dob, string id, int stNumber, string stName, string postCode, string city, string phone1, string phone2, string email)
        {
            var uid = int.Parse(id);
            var date = DateTime.Parse(dob);

            var customdb = new AddPerson(fname, lname, date, uid, stNumber, stName, postCode, city, phone1, phone2, email);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //UPDATE TABLE-NAME SET COLUMN = VALUE WHERE COLUMN = VALUE

            updateCommand.CommandText = "UPDATE tbl_people SET FNAME = @fname, LNAME = @lname, DOB = @dob, Str_NUMBER = @stNumber, Str_NAME = @stName, POSTCODE = @postCode, CITY = @city, PHONE1 = @phone1, PHONE2 = @phone2, EMAIL = @email WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@fname", customdb.fname);
            updateCommand.Parameters.AddWithValue("@lname", customdb.lname);
            updateCommand.Parameters.AddWithValue("@dob", customdb.dob);
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.Parameters.AddWithValue("@stNumber", customdb.stNumber);
            updateCommand.Parameters.AddWithValue("@stName", customdb.stName);
            updateCommand.Parameters.AddWithValue("@postCode", customdb.postCode);
            updateCommand.Parameters.AddWithValue("@city", customdb.city);
            updateCommand.Parameters.AddWithValue("@phone1", customdb.phone1);
            updateCommand.Parameters.AddWithValue("@phone2", customdb.phone2);
            updateCommand.Parameters.AddWithValue("@email", customdb.email);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }

        ////////////////////  DELETE STATEMENTS ///////////////////

        //CHANGE THIS!!
        //The Insert Statement needs to be customised
        public static void DeleteData(string id)
        {
            var uid = int.Parse(id);

            var customdb = new AddPerson(uid);

            var con = conn();
            con.Open();

            MySqlCommand updateCommand = con.CreateCommand();

            //DELETE FROM TABLE-NAME WHERE COLUM = VALUE

            updateCommand.CommandText = "DELETE FROM tbl_people WHERE ID = @id";
            updateCommand.Parameters.AddWithValue("@id", customdb.id);
            updateCommand.ExecuteNonQuery();

            con.Clone();
        }
    }
}
