﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using MySql.Data.MySqlClient;
using MySQLDemo.Classes;
using Windows.UI.Popups;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task01_uwp
{
    public sealed partial class MainPage : Page
    {
        static string userAccount = "";

        /******************** UI LOGIC ********************/
        public MainPage()
        {
            this.InitializeComponent();
            //It loads the list with surname, name
            names.ItemsSource = loadLnameFname();
            //It shows initial instructions
            message1.Text = mi;
            message2.Text = msi;
        }

        //It clears all the objects
        void clearAll()
        {
            fname.Text = "First Name";
            lname.Text = "Last Name";
            dob.Text = "Date of Birth";
            stNumber.Text = "St. Number";
            stName.Text = "St. Name";
            postCode.Text = "Post Code";
            city.Text = "City";
            phone1.Text = "Phone 1";
            phone2.Text = "Phone 2";
            email.Text = "Email";
            //It clears the selection
            names.SelectedItem = -1;
            userAccount = "";
        }

        //It shows the initial messages
        private void showInitialMessages()
        {
            //It shows initial messages in the message bar
            message1.Text = mi;
            message2.Text = msi;
        }

        //It saves shows all of the contact data in the form
        private void names_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            splitter.IsPaneOpen = !splitter.IsPaneOpen;
            //It checks if the object still exists
            if (names.SelectedItem != null)
            {
                var b=selectedName(names.SelectedItem.ToString());
                userAccount = b[0];
                fname.Text = b[1];
                lname.Text = b[2];
                dob.Text = b[3];
                stNumber.Text = b[4];
                stName.Text = b[5];
                postCode.Text = b[6];
                city.Text = b[7];
                phone1.Text = b[8];
                phone2.Text = b[9];
                email.Text = b[10];
            }
            //It selects the first pivot tab
            rootPivot.SelectedIndex = 0;
            //It shows execution messages in the message bar
            message1.Text = me;
            message2.Text = mse;
        }

        //It opens/closes the panel and shows the current panel status
        private void hamburguerGeneral_Click(object sender, RoutedEventArgs e)
        {
            splitterActions(m1, ms1);
        }

        //It opens/closes the panel when searching contacts and loads the corresponding data
        private void hamburguerSearch_Click(object sender, RoutedEventArgs e)
        {
            splitterActions(m2, ms2);
            nameSearch.SelectAll();
        }

        //It opens/closes the panel when selecting contacts and loads the corresponding data
        private void hamburguerSelect_Click(object sender, RoutedEventArgs e)
        {
            splitterActions(m3, ms3);
        }

        //It takes action on the splitview and the messages area depending on the status
        private void splitterActions(string menu, string message)
        {
            if (splitter.IsPaneOpen)
            {
                message1.Text = mi;
                message2.Text = msi;
                splitter.IsPaneOpen = false;
            }
            else
            {
                message1.Text = menu;
                message2.Text = message;
                nameSearch.Text = ms4;
                names.ItemsSource = loadLnameFname();
                splitter.IsPaneOpen = true;
            }

        }
        //It searches in the DB the name or surname written in the textbox and loads the list
        private void nameSearch_TextChanging(TextBox sender, TextBoxTextChangingEventArgs args)
        {
            names.ItemsSource = searchingName($"%{nameSearch.Text}%");
        }

        //It adds Information to the DB
        private void addToDB_Click(object sender, RoutedEventArgs e)
        {
            var dataOK = checkAllFields(fname.Text, lname.Text, dob.Text, stNumber.Text, stName.Text, postCode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
            if (dataOK==1) executeDBAction("add", fname.Text, lname.Text, dob.Text, stNumber.Text, stName.Text, postCode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
            showInitialMessages();
        }

        //It updates Information in the DB after checking all data introduced is ok and user confirmation
        private void updateDB_Click(object sender, RoutedEventArgs e)
        {
            var dataOK = checkAllFields(fname.Text, lname.Text, dob.Text, stNumber.Text, stName.Text, postCode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
            if (dataOK == 1) showRequestDialog("update");
        }

        //It deletes Information from the DB after user confirmation
        private void removeFromDB_Click(object sender, RoutedEventArgs e)
        {
            if (userAccount == "")
            {
                showAlert2Dialog();
            }
            else showRequestDialog("delete");
            showInitialMessages();
        }

        //Dialog 1 asking the user to confirm action
        private async void showRequestDialog(string action)
        {
            var dialog1 = new MessageDialog(info1);
            dialog1.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            dialog1.Commands.Add(new UICommand { Label = "Cancel", Id = 1 });
            var dialog1Action = await dialog1.ShowAsync();
            dialog1.DefaultCommandIndex = 1;
            dialog1.CancelCommandIndex = 1;
            takeActionsFromDialog1Selection((int)dialog1Action.Id, action, fname.Text, lname.Text, dob.Text, stNumber.Text, stName.Text, postCode.Text, city.Text, phone1.Text, phone2.Text, email.Text);
        }

        //Dialog 2 asking the user to fill in correctly all fields
        private async void showAlertDialog()
        {
            var dialog2 = new MessageDialog(info2);
            dialog2.Commands.Add(new UICommand { Label = "OK" });
            var dialog3Action = await dialog2.ShowAsync();
        }

        //Dialog 3 confirming the user executed action
        private async void showConfirmationDialog()
        {
            var dialog3 = new MessageDialog(info3);
            dialog3.Commands.Add(new UICommand { Label = "OK" });
            var dialog3Action = await dialog3.ShowAsync();
        }

        //Dialog 4 alerting the user to select a contact before pressing remove button
        private async void showAlert2Dialog()
        {
            var dialog3 = new MessageDialog(info4);
            dialog3.Commands.Add(new UICommand { Label = "OK" });
            var dialog3Action = await dialog3.ShowAsync();
        }


        /******************** PROGRAM LOGIC ********************/
        //UI Messages
        static string info1 = "Are you sure?";
        static string info2 = "Please fill in all fields with correct data";
        static string info3 = "Action executed";
        static string info4 = "Please select a contact first";
        static string mi = "Options";
        static string m1 = "Menu";
        static string m2 = "Search";
        static string m3 = "Select";
        static string me = "Execute";
        static string msi = "Please fill in all the fields to add a new contact or open the side menu for more options";
        static string ms1 = "Please select a contact in the list or write a Name or a Surname to be searched";
        static string ms2 = "Please write a Name or Surname to be searched in the contacts";
        static string ms3 = "Please select contact from the list to load the data";
        static string ms4 = "Search a Name";
        static string mse = "Update, remove or add the contact pressing the buttons or open the side menu for more options";

        //It searches if the written name or surname in the TextBox is in the DB
        static List<string> searchingName(string text)
        {
            var command = $"Select FNAME, LNAME from tbl_people WHERE FNAME LIKE '{text}' OR LNAME LIKE '{text}'";
            return MySQLCustom.ShowLnameNameInList(command);
        }

        //It makes a query to look for the data of the selected contact
        static List<string> selectedName(string name)
        {
            //It extracts the surname from the list selected item
            var surname = name.Remove(name.IndexOf(','), name.Length - name.IndexOf(','));
            //It extracts the first name from the list selected item
            var firstname = name.Replace(surname + ", ", "");
            //It searches in the DB the register with selected first name and surname
            var command = $"Select * from tbl_people WHERE FNAME = '{firstname}' and LNAME = '{surname}'";
            return MySQLCustom.ShowInList(command);
        }

        //It makes a query to look for all the DB names and surnames
        static List<string> loadLnameFname()
        {
            var command = $"Select FNAME, LNAME from tbl_people";
            var list = MySQLCustom.ShowLnameNameInList(command);
            return list;
        }

        //It checks that all the fields have been filled in with correct data and launches the Dialog1
        private int checkAllFields(string fname, string lname, string dobText, string stNumberText, string stName, string postCode, string city, string phone1, string phone2, string email)
        {
            var result = 0;
            var stNumber = 0;
            DateTime stDate;
            var numberConversionResult =int.TryParse(stNumberText, out stNumber);
            var dateConversionResult = DateTime.TryParse(dobText, out stDate);
            if (!numberConversionResult || !dateConversionResult || fname == "" || lname == "" || stDate == null || stNumber == 0 || stName == "" || postCode == "" || city == "" || phone1 == "" || phone2 == "" || email == "")
            {
                result = 0;
                showAlertDialog();
            }
            else result = 1;
            return result;
        }

        //It acts depending on Dialog1 selection
        private void takeActionsFromDialog1Selection(int dialogSelection, string action, string fname, string lname, string dobText, string stNumberText, string stName, string postCode, string city, string phone1, string phone2, string email)
        {
            switch (dialogSelection)
            {
                case 0:
                    executeDBAction(action, fname, lname, dobText, stNumberText, stName, postCode, city, phone1, phone2, email); 
                    break;
                case 1:
                    break;
            }
        }

        //It executes the corresponding DB commands depending on action selected by the user
        private void executeDBAction(string action, string fname, string lname, string dobText, string stNumberText, string stName, string postCode, string city, string phone1, string phone2, string email)
        {
            switch (action)
            {
                //It adds the data to the DB
                case "add":
                    MySQLCustom.AddData(fname, lname, dobText, int.Parse(stNumberText), stName, postCode, city, phone1, phone2, email);
                    clearAll();
                    break;
                //It updates the data in the DB
                case "update":
                    MySQLCustom.UpdateData(fname, lname, dobText, userAccount, int.Parse(stNumberText), stName, postCode, city, phone1, phone2, email);
                    break;
                //It deletes the data from the DB
                case "delete":
                    MySQLCustom.DeleteData(userAccount);
                    clearAll();
                    break;
            }
            //It loads the list of contacts
            names.ItemsSource = loadLnameFname();
            //It confirms that user actions have been done
            showConfirmationDialog();
            //It closes the pane
            splitter.IsPaneOpen = false;
        }
    }
}
